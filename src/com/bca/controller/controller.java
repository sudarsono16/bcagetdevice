package com.bca.controller;

import org.apache.logging.log4j.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.DeviceManagementAdapter;
import adapter.ErrorAdapter;
import adapter.LogAdapter;
import model.mdlAPIResult;
import model.mdlBranch;
import model.mdlBranchList;
import model.mdlDeviceManagement;
import model.mdlErrorSchema;
import model.mdlLog;

@RestController
public class controller {
    final static Logger logger = LogManager.getLogger(controller.class);
    static Gson gson = new Gson();

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody void GetPing() {
        return;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public @ResponseBody mdlAPIResult GetDeviceManagement(@RequestParam(value = "serial", defaultValue = "") String SerialNumber) {
        long startTime = System.currentTimeMillis();
        mdlAPIResult mdlGetDeviceManagementResult = new mdlAPIResult();
        mdlDeviceManagement DeviceManagement = new mdlDeviceManagement();
        mdlErrorSchema mdlErrorSchema = new mdlErrorSchema();

        mdlLog mdlLog = new mdlLog();
        mdlLog.WSID = "";
        mdlLog.SerialNumber = SerialNumber;
        mdlLog.ApiFunction = "getDeviceManagement";
        mdlLog.SystemFunction = "GetDeviceManagement";
        mdlLog.LogSource = "Webservice";
        mdlLog.LogStatus = "Failed";
        try {
            DeviceManagement = DeviceManagementAdapter.GetDeviceManagement(SerialNumber);
            if (DeviceManagement.SerialNumber == null) {
                mdlErrorSchema = ErrorAdapter.GetErrorSchema("01");
                mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
                mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, "/BCAGetDevice/get", "GET", "serial: "
                        + SerialNumber, "", gson.toJson(mdlGetDeviceManagementResult)));
            } else {
                if (DeviceManagement.ServiceHour.ServiceHour1 == null || DeviceManagement.ServiceHour.ServiceHour1.equals("")) {
                    DeviceManagement = DeviceManagementAdapter.insertDeviceServiceHourData(DeviceManagement);
                }
                // check to table ms_branch in database if branch data is exists
                boolean exists = DeviceManagementAdapter.CheckBranchFromDatabase(DeviceManagement);
                if (exists) {
                    // if exists, then check last updated date of branch data
                    String lastUpdatedDate = DeviceManagementAdapter.GetLastUpdatedDateBranch(DeviceManagement);
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date dateNow = new Date();
                    String dateNowString = dateFormat.format(dateNow);

                    if (lastUpdatedDate.equals("") || !lastUpdatedDate.equals(dateNowString)) {
                        // if last updated date is not today, check branch data from API to EAI
                        mdlAPIResult getEAIBranchResult = DeviceManagementAdapter.hitApiInquiryBranch(DeviceManagement);

                        if (getEAIBranchResult == null || getEAIBranchResult.ErrorSchema == null) {
                            // mdlErrorSchema = ErrorAdapter.GetErrorSchema("03");
                            mdlErrorSchema = ErrorAdapter.GetErrorSchema("00");
                            mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
                            mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                            DeviceManagement.BranchCode = "-";
                            DeviceManagement.BranchName = "-";
                            DeviceManagement.BranchTypeID = "-";
                            DeviceManagement.BranchInitial = "-";
                            mdlGetDeviceManagementResult.OutputSchema = DeviceManagement;
                            logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, "/BCAGetDevice/get", "GET", "serial: "
                                    + SerialNumber, "", gson.toJson(mdlGetDeviceManagementResult)));
                        } else {
                            String getEAIBranchJson = gson.toJson(getEAIBranchResult);
                            if (!getEAIBranchResult.ErrorSchema.ErrorCode.equals("ESB-00-000")) {
                                // mdlErrorSchema = getEAIBranchResult.ErrorSchema;
                                mdlErrorSchema = ErrorAdapter.GetErrorSchema("00");
                                mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
                                mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                                DeviceManagement.BranchCode = "-";
                                DeviceManagement.BranchName = "-";
                                DeviceManagement.BranchTypeID = "-";
                                DeviceManagement.BranchInitial = "-";
                                mdlGetDeviceManagementResult.OutputSchema = DeviceManagement;
                                logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, "/BCAGetDevice/get", "GET", "serial: " + SerialNumber
                                        + ", getEAIBranchResult: " + getEAIBranchJson, "", gson.toJson(mdlGetDeviceManagementResult)));
                            } else {
                                String branchDataString = gson.toJson(getEAIBranchResult.OutputSchema);
                                mdlBranchList branchList = gson.fromJson(branchDataString, mdlBranchList.class);
                                if (branchList == null || branchList.branch.size() == 0) {
                                    // there is no branch data in EAI
                                    mdlErrorSchema = ErrorAdapter.GetErrorSchema("00");
                                    mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
                                    mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                                    DeviceManagement.BranchCode = "-";
                                    DeviceManagement.BranchName = "-";
                                    DeviceManagement.BranchTypeID = "-";
                                    DeviceManagement.BranchInitial = "-";
                                    mdlGetDeviceManagementResult.OutputSchema = DeviceManagement;
                                    logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, "/BCAGetDevice/get", "GET", "serial: " + SerialNumber
                                            + ", getEAIBranchResult: " + getEAIBranchJson, "", gson.toJson(mdlGetDeviceManagementResult)));
                                } else {
                                    String branchCode = DeviceManagement.BranchCode;
                                    String branchType = DeviceManagement.BranchTypeID;
                                    String branchInitial = DeviceManagement.BranchInitial;
                                    // search branch data from branch data list EAI returned
                                    mdlBranch mdlBranch = branchList.branch.stream().filter(branch -> branch.branch_code.substring(0, 4).equals(branchCode)
                                            && branch.branch_type.equalsIgnoreCase(branchType)
                                            && branch.branch_initial.equalsIgnoreCase(branchInitial)).findAny().orElse(null);
                                    if (mdlBranch == null) {
                                        mdlErrorSchema = ErrorAdapter.GetErrorSchema("00");
                                        mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
                                        mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                                        DeviceManagement.BranchCode = "-";
                                        DeviceManagement.BranchName = "-";
                                        DeviceManagement.BranchTypeID = "-";
                                        DeviceManagement.BranchInitial = "-";
                                        DeviceManagement.BranchTypeName = "-";
                                        mdlGetDeviceManagementResult.OutputSchema = DeviceManagement;
                                        logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, "/BCAGetDevice/get", "GET", "serial: "
                                                + SerialNumber
                                                + ", getEAIBranchResult: " + getEAIBranchJson, "", gson.toJson(mdlGetDeviceManagementResult)));
                                    } else {
                                        // trim branch code to only 4 digits (in case branch code is more than 4 digit ex :0021KBK
                                        mdlBranch.branch_code = mdlBranch.branch_code.substring(0, 4);
                                        // if branch data in EAI match with branch data from database app server, then update database branch
                                        // data from EAI branch data
                                        boolean successUpdateBranchData = DeviceManagementAdapter.UpdateBranchData(mdlBranch, DeviceManagement.WSID);
                                        if (!successUpdateBranchData) {
                                            mdlErrorSchema = ErrorAdapter.GetErrorSchema("00");
                                            mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
                                            mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                                            DeviceManagement.BranchCode = "-";
                                            DeviceManagement.BranchName = "-";
                                            DeviceManagement.BranchTypeID = "-";
                                            DeviceManagement.BranchInitial = "-";
                                            DeviceManagement.BranchTypeName = "-";
                                            mdlGetDeviceManagementResult.OutputSchema = DeviceManagement;
                                            logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, "/BCAGetDevice/get", "GET", "serial: "
                                                    + SerialNumber
                                                    + ", getEAIBranchResult: " + getEAIBranchJson, "", gson.toJson(mdlGetDeviceManagementResult)));
                                        } else {
                                            // use branch name from EAI in case branch name changes in EAI
                                            DeviceManagement.BranchName = mdlBranch.branch_name;
                                            mdlGetDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("00");
                                            mdlGetDeviceManagementResult.OutputSchema = DeviceManagement;
                                            mdlLog.LogStatus = "Success";
                                            mdlLog.WSID = DeviceManagement.WSID;
                                            logger.info(LogAdapter.logControllerToLog4j(true, startTime, 200, "/BCAGetDevice/get", "GET", "serial:"
                                                    + SerialNumber + ", WSID : " + DeviceManagement.WSID, "", gson.toJson(mdlGetDeviceManagementResult)));
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        // if last updated date is today, then use device and branch data from database as result
                        mdlGetDeviceManagementResult.ErrorSchema = ErrorAdapter.GetErrorSchema("00");
                        mdlGetDeviceManagementResult.OutputSchema = DeviceManagement;
                        mdlLog.LogStatus = "Success";
                        mdlLog.WSID = DeviceManagement.WSID;
                        logger.info(LogAdapter.logControllerToLog4j(true, startTime, 200, "/BCAGetDevice/get", "GET", "serial:"
                                + SerialNumber + ", WSID : " + DeviceManagement.WSID, "", gson.toJson(mdlGetDeviceManagementResult)));
                    }
                } else {
                    mdlErrorSchema = ErrorAdapter.GetErrorSchema("00");
                    mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
                    mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                    DeviceManagement.BranchCode = "-";
                    DeviceManagement.BranchName = "-";
                    DeviceManagement.BranchTypeID = "-";
                    DeviceManagement.BranchInitial = "-";
                    DeviceManagement.BranchTypeName = "-";
                    mdlGetDeviceManagementResult.OutputSchema = DeviceManagement;
                    logger.error(LogAdapter.logControllerToLog4j(false, startTime, 400, "/BCAGetDevice/get", "GET", "serial:" + SerialNumber
                            + ", DeviceManagement : " + gson.toJson(DeviceManagement), "", gson.toJson(mdlGetDeviceManagementResult)));
                }
            }
        } catch (Exception ex) {
            mdlErrorSchema = ErrorAdapter.GetErrorSchema("02");
            mdlLog.ErrorMessage = mdlErrorSchema.ErrorMessage.English;
            mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;
            logger.error(LogAdapter.logControllerToLog4jException(startTime, 500, "/BCAGetDevice/get", "GET", "serial:"
                    + SerialNumber, "", gson.toJson(mdlGetDeviceManagementResult), ex.toString()), ex);
        }

        LogAdapter.InsertLog(mdlLog);
        return mdlGetDeviceManagementResult;
    }

}
